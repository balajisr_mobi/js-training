//addEventListener()
// var para = document.getElementById("para");
// para.addEventListener("click", function(e) {
//     e.target.innerText = "Hi"
//     console.log(e);
// })

//removeEventListener()
// var para = document.getElementById("para");
// function onclicK(e) {
//     e.target.innerText = "Hi"
//     console.log(e);
// }

// para.addEventListener("click", onclicK)
// para.removeEventListener("click", onclicK)

//e

//Event Bubbling
// var outer = document.querySelector(".outer");
// var middle = document.querySelector(".middle");
// var inner = document.querySelector(".inner");

// outer.addEventListener("click", function(e) {
//     console.log("outer")
//     e.stopPropagation();
// })
// middle.addEventListener("click", function(e) {
//     console.log("middle")
//     e.stopPropagation();
// })
// inner.addEventListener("click", function(e) {
//     console.log("inner")
//     e.stopPropagation();
// })


//Event Capturing { "capture": true }
// var outer = document.querySelector(".outer");
// var middle = document.querySelector(".middle");
// var inner = document.querySelector(".inner");

// outer.addEventListener("click", function(e) {
//     console.log("outer")
//     e.stopPropagation();
// }, { "capture": true })
// middle.addEventListener("click", function(e) {
//     console.log("middle")
//     e.stopPropagation();
// }, { "capture": true })
// inner.addEventListener("click", function(e) {
//     console.log("inner")
//     e.stopPropagation();
// }, { "capture": true })

//{ "once": true }
// var outer = document.querySelector(".outer");
// outer.addEventListener("click", function(e) {
//         console.log("inner")
// }, { "once": true })

//e.stopProporation()

//Event Delegation
var div = document.querySelectorAll("div");
for(let d of div) {
    d.addEventListener("click", function() {
        console.log("clicked")
    })
}

var newDiv = document.createElement("div")
newDiv.innerText = "something";
document.body.append(newDiv);