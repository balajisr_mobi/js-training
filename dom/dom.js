//append()
// let para = document.getElementById("para");
// let anchor = document.createElement("a");
// anchor.href = "http://google.com";
// para.append(anchor, "something");

//appendChild()
// let para = document.getElementById("para");
// let anchor = document.createElement("a");
// anchor.href = "http://google.com";
// para.appendChild(anchor);

//createElement()

//innerText
// var para = document.getElementById("para")
// para.innerText = "<p>something<p>"

//innerHTML
// var para = document.getElementById("para")
// para.innerHTML = "<h1>something<h1>"

//textContent
// var para = document.getElementById("para")
// console.log(para.textContent)

//remove()
// var para = document.getElementById("para")
// para.remove()

//getAttribute()
// var para = document.getElementById("para")
// console.log(para.getAttribute("id"));

//setAttribute()
// var para = document.getElementById("para")
// console.log(para.setAttribute("class", "paragraph"));

//removeAttribute()
// var para = document.getElementById("para")
// console.log(para.removeAttribute("id"));

//dataset
// var para = document.getElementById("para")
// console.log(para.dataset);

//classList, add(), remove(), toggle()
// var para = document.getElementById("para")
// para.classList.toggle("some")
// console.log(para.classList);

//style
var para = document.getElementById("para")
para.style.color = "red"; //single style
para.style.cssText = "display: block; position: absolute"; //multiple style