// # Instructions
// - use spread & rest operators to make finally return 'true'
// - do not change lines marked with star comment

function foo() { 
}

function bar() {
	var a1 = [ 2, 4 ]; // *
	var a2 = [ 6, 8, 10, 12 ]; // *

	return foo();
}

console.log( // *
	bar().join("") === "281012" // *
); // *