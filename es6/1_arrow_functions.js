//simple arrow function
function foo() {
    return 3;
}
foo = () => 3; //no function name, no return keyword


//anonymous function
function foo() {
    return 3;
}
() => 3;


//gather or spread operator
function foo(...x) {
    return 3;
}
(...x) => 3;


//with one parameter
function foo(x) {
    return x + 3;
}
foo = x => x + 3;


//multiple parameters
function foo(x,y) {
    return x + y;
}
(x,y) => x + y;


//Note: In all the above examples, function body doesn't have '{ }' and it is allowed for expressions but not statements
//statements(if, try catch, etc...) need {}
x => { try {3} catch(e) {console.log(e)} }
x => { if(x > 9) return true }
x => { return 3 } //when function body uses {}, then return keyword is necessary if it needs


//returning an object
function foo(x) {
    return {
        y: x
    }
}
x => ({ y: x })


//one of the negatives about arrow function is, because of its variations
//it may take less time to write, but more time for readability because of different variations


//this keyword problem
var obj = {
    id: 42,
    foo: function foo() {
        setTimeout(function() {
            console.log(this.id)
        },100)
    }
}
obj.foo() // undefined (as 'this' refers to global object)

//to solve the above issue, there are three ways
//1. using variable
var obj = {
    id: 42,
    foo: function foo() {
        var context = this;
        setTimeout(function() {
            console.log(context.id)
        }, 100)
    }
}
obj.foo() //42
//2. using bind
var obj = {
    id: 42,
    foo: function foo() {
        setTimeout(function() {
            console.log(this.id)
        }.bind(this), 100)
    }
}
obj.foo() //42
//3. using arrow function
var obj = {
    id: 42,
    foo: function foo() {
        setTimeout(() => {
            console.log(this.id)
        }, 100)
    }
}
obj.foo() //42