//default values
//old
function foo(x) {
    x = x || 42;
    console.log(x)
}
foo() // 42
foo(5) // 5
foo(0) // 42

function foo(x) {
    x = x !== undefined ? x : 42;
    console.log(x);
}
foo(0) // 0

//new
function foo(x = 42) {
    console.log(x)
}
foo() // 42
foo(5) // 5
foo(0) // 0


//lazy expression
function bar() {
    console.log("!")
}
function foo(x = bar()) {
    console.log(x)
}


//lazy expression use cases
//1
function createId() {
    return new Date().getTime()
}
function foo(x = createId()) {
    console.log(x)
}
//2
function requiredField() {
    throw "Parameter required"
}
function foo(x = requiredField()) {
    console.log(x)
}


//passing lazy expression values
function createId() {
    return new Date().getTime()
}
function foo(x = createId(), y = x) {
    console.log(x,y)
}


//lazy expression return
var x = 1;
function foo( x = 3, f = function() {return x} ) {
    console.log(f())
}
foo() // 3

function foo( x = 3, f = function() {return x} ) {
    var x = 5;
    console.log(f())
}
foo() // 3