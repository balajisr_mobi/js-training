//Symbol
var x = Symbol() //some unique value under the hood


//Symbol with description
var x = Symbol( "this is a description" )
console.log(x) // Symbol(this is a description)


//Symbol use cases
var x = Symbol( "unique value" )
var obj = { x:42 }
obj[x] = 'this is secret'
console.log(obj) // { x: 42, Symbol(unique value): 'this is a secret' }

Object.keys(obj) // ['x']
Object.getOwnPropertyNames(obj) // ['x']
Object.getOwnPropertySymbols(obj) // [Symbol(unique value)]


//iterators
var arr = [1,2,3]

var it = arr[Symbol.iterator]()

it.next(); // { value:1, done:false }
it.next(); // { value:2, done:false }
it.next(); // { value:3, done:false }
it.next(); // { value:undefined, done:true }


//for...of
var arr = [1,2,3]

for(var a of arr) {
    console.log(a)
}


//for...of (strings)
var str = "hello"

for(var s of str) {
    console.log(s)
}


//for...in
//this is not an iterator
var str = "hello"

for(var k in str) {
    console.log(str[k])
}