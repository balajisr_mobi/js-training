//function arguments
function foo() {
    console.log(arguments)
}
foo('a','b','c','d') // Arguments['a','b','c','d']
//problem with this Arguments is you can't perform array functionalities

function foo() {
    var args = [].slice.call( arguments );
    args.unshift( 42 );
    bar.apply( null, args );
}


//rest/gather or spread operator
function foo( ...args ) {
    args.unshift( 42 );
    bar( ...args );
}

function foo( ...args ) {
    bar( 42, ...args );
}


//concatenation
var x = [1,2]
var y = [3,4,5]
var z = [0].concat(x,y,[6])
console.log(z) // [0,1,2,3,4,5,6]

var x = [1,2]
var y = [3,4,5]
var z = [0, ...x, ...y, 6]
console.log(z) // [0,1,2,3,4,5,6]


//spread on strings
var x = 'hello'
console.log(...x) // h e l l o


//tricky example
function foo(x,y,...rest) {
    console.log(x)
    console.log(y)
    return rest
}
var a = [1,2,3]
var b = [4,5,6]
foo( ...a, ...b ) // 1 2 [3,4,5,6]