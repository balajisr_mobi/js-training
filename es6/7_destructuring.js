//destructuring
var tmp = [1,2,3]
var a = tmp[0]
var b = tmp[1]
var c = tmp[2]

var tmp = [1,2,3]
var [a,b,c] = tmp //a = 1, b = 2, c = 3

var tmp = [1,2]
var [a,b,c] = tmp //a = 1, b = 2, c = undefined

var tmp = [1,2,3,4]
var [a,b,c] = tmp //a = 1, b = 2, c = 3


//with default values
var tmp = [1,2,3]
var a = tmp[0]
var b = tmp[1]
var c = tmp[2] !== undefined ? tmp[2] : 42

var tmp = [1,2,3]
var [a,b,c=42] = tmp //a = 1, b = 2, c = 3


//null
var tmp = null
var a = tmp[0] //Error
var b = tmp[1]
var c = tmp[2]

var tmp = null
var [a,b,c] = tmp //Error

var [a,b,c] = tmp || [] //this will guard against error


//spread
var tmp = [1,2,3,4,5,6]
var [a,b,c,...args] = tmp //a = 1, b = 2, c = 3, args = [4,5,6]


//variable assignment not declaration
var a,b,c,args,tmp;
tmp = [1,2,3,4,5,6];
[a,b,c,...args] = tmp //a = 1, b = 2, c = 3, args = [4,5,6]


//object properties
var obj = {};
tmp = [1,2,3,4,5,6];
[obj.a, obj.b, obj.c, ...obj.args] = tmp //obj.a = 1, obj.b = 2, obj.c = 3, obj.args = [4,5,6]


//swap
var x = 10, y = 20;
[x, y] = [y, x]; // x = 20, y = 10

var a = [1,2,3];
[x, y, ...a] = [0, ...a, 4]; // x = 0, y = 1, a = [2,3,4]

var a = [1,2,3];
[ , , ...a] = [0, ...a, 4] // a = [2,3,4]


//nested destructuring
var tmp = [1,2,3, [4,5,6] ]
var [a,b,c, [d, , e] ] = tmp //a = 1, b = 2, c = 3, d = 4, e = 6


//destructuring is not array
var tmp = [1,2,3, [4,5,6] ]
var a,b,c,d,e;
var x = [a,b,c, [d, , e] ] = tmp // x = [1,2,3, [4,5,6] ]


//multiple destructuring
var tmp = [1,2,3, [4,5,6] ]
var [ , , , [ d, , e] ] = [a,b,c] = tmp


//object destructuring
var tmp = { a:1, b:2, c:3 }
var { a:x, b:y, c:z } = tmp // x = 1, y = 2, z = 3


//object destructuring with default values
var tmp = { a:1, c:3 }
var { a:x, b:y=42, c:z } = tmp // x = 1, y = 42, z = 3


//object destructuring with falsy value
var tmp = null
var { a:x, b:y, c:z } = tmp || {} // this will guard against falsy value