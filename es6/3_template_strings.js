//conscise property
var a = 1;
var obj = {
    a: a
}

var a = 1;
var obj = {
    a
}

//conscise method
var obj = {
    fn: function() {
        //something
    },
    'some fn': function() {
        //something
    }
}

var obj = {
    fn() {
        //something
    },
    'some fn'() {
        //something
    }
}

//computed property names
var c = 'hello'
var obj = {}
obj[c] = 42;
obj[c+'fn'] = function() {}

var c = 'hello'
var obj {
    [c.toUpperCase()]: 42,
    [c+'fn']() { }
}


//template literals
var name = 'jack'
var age = 18
var place = 'new york'

var sentence = 'My name is ' + name + ' and I\'m ' + age + ' years old who lives in ' + place + '.';
var sentence = `My name is ${name} and I'm ${age} years old who lives in ${place}.`