//block usecases
function foo(x, y) {
    if(x > y) {
        var tmp = x; //can avoid being hoisted to foo function
        x = y;
        y = tmp;
    }

    for(var i=0; i<10; i++) { //now 'i' is available throughout foo function
        //something
    }
}


//for loops
function foo() {
    for(var i=0; i<10; i++) { //var will override other i values, but let will create seperate i values
        $('#btn'+i).click(function() {
            console.log(i + 'clicked!')
        })
    }
}


//const - variable that cannot be reassigned
const x = 3;

const y = [1,2,3];
y[0] = 4;
y.push(5);


//to freeze a value
const y = Object.freeze([1,2,3]);