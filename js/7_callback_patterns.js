//split callback
function trySomething(ok, err) {
    setTimeout(function() {
        var num = Math.random()
        if(num > 0.5) ok(num);
        else err(num);
    },1000)
}
trySomething(
    function(num) {
        console.log('success: ' + num);
    },
    function(num) {
        console.log('sorry: ' + num)
    }
)


//error-first callback
function trySomething(cb) {
    setTimeout(function() {
        var num = Math.random()
        if(num > 0.5) cb(null,num);
        else cb('Too low!')
    },1000)
}
trySomething(function(err, num) {
    if(err) {
        console.log(err)
    } else {
        console.log('Num: ' + num)
    }
})