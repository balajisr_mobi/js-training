//simple callback example
//handling concurrency in async operation

function test(cb) {
    fetch('https://jsonplaceholder.typicode.com/todos/1')
        .then(response => response.json())
        .then(data => {
            //console.log(data);
            cb(data)
        });
}

function result() {
    test(function (callbackData) {
        console.log(callbackData)
    });
}

result()