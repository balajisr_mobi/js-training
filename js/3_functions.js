//javascript functions are also called first-order functions
//that is, functions can act like values

//functions and variables
var fn = function() {
    return 9;
}
var num = 9;
console.log(num === fn())

//functions as arguments
function some(x, y) {
    console.log(x);
    y(x);
}
some(5, function(n) {
    console.log(n + 10)
})

//functions as return value
function any() {
    function inner() {
        console.log('hello')
    }
    return inner;
}
any()