//important array methods

//remove elements in array (slice & splice)
var arr = [1,2,3,4,5]
var myArr = [1,2,3,4,5]

var arr1 = arr.splice(2,1) //2 is the index & 1 is number of elements to remove
console.log(arr1, arr) //splice will return new array & also affect existing array

var myArr1 = myArr.slice(2,1) //remove from start index to end index
console.log(myArr1, myArr) //slice will return new array but not affect existing array


//loop all elements and return new array
//map()
const array1 = [1, 4, 9, 16];
const map1 = array1.map(x => x * 2); // pass a function to map
console.log(map1); // expected output: Array [2, 8, 18, 32]

//filter()
const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
const result = words.filter(word => word.length > 6);
console.log(result); // expected output: Array ["exuberant", "destruction", "present"]

//reduce()
const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;
console.log(array1.reduce(reducer)); // 1 + 2 + 3 + 4 // expected output: 10
console.log(array1.reduce(reducer, 5)); // 5 + 1 + 2 + 3 + 4 // expected output: 15