//async operations starts only after performing all synchronous operations in a file
//async in javascript is part of WEB API's

setTimeout(function() {
    console.log(1)
},0)

setTimeout(function() {
    console.log(2)
},1000)

console.log(3)

//output
// 3
// 1
// 2