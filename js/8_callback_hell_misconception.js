//callback hell often has misconception
//one of the common misconception is 'callback hell is about indendation'

setTimeout(function() {
    console.log('one')
    setTimeout(function() {
        console.log('two')
        setTimeout(function() {
            console.log('three')
        },1000)
    },1000)
},1000)

//the above code is callback hell, but the reason is not because of indentation

function one(cb) {
    console.log('one')
    setTimeout(cb, 1000)
}
function two(cb) {
    console.log('two')
    setTimeout(cb, 1000)
}
function three() {
    console.log('three')
}
one(function() {
    two(three)
})

//this above code is same as first one, but without indendation issues