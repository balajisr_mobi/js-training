//why global namespacing is bad?
//because it will create variable name conflicts and increases application memory

//to avoid these name conflicts IIFE was used before ES6
//this will put everything inside function scope and also executes file immediately
(function(){
    var a = 'hello'

    function something() {
        console.log('world')
    }
})()


//from ES6 onwards, we have let and const to declare variables
let a = 'hello' //not global scope
const PORT = 9000 //not global scope and can't change primitive values