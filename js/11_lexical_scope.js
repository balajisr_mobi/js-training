//Exercise 1
var foo = 'bar'
function bar() {
    var foo = 'baz'
}
function baz(foo) {
    foo = 'bam'
    bam = 'yay'
}
bam // ???
baz() // ???
bam // ???
foo // ???


//Exercise 2
var foo = 'bar'
function bar() {
    var foo = 'baz'
    function baz(foo) {
        foo = 'bam'
        bam = 'yay'
    }
    baz()
}
bar()
foo // ???
bam // ???
baz() // ???