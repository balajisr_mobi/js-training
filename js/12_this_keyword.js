//if lexical scope is author time decision, this is kind of dyncmic scoping
//it has four rules with order of precedence

//4.default binding
function foo() {
    console.log(this.bar)
}
var bar = 'bar1'


//3.implicit binding
function foo() {
    console.log(this.bar)
}
var bar = 'bar1'
var o2 = { bar:"bar2", foo: foo }
var o3 = { bar: "bar3", foo: foo }
foo() // 'bar1'
o2.foo() // 'bar2'
o3.foo() // 'bar3'

//***

var o1 = {
    bar: 'bar1',
    foo: function() {
        console.log(this.bar)
    }
}
var o2 = { bar: 'bar2', foo: o1.foo }
var bar = 'bar3'
var foo = o1.foo
o1.foo() //'bar1'
o2.foo() //'bar2'
foo() //'bar3'


//2.explicit binding
function foo() {
    console.log(this.bar)
}
var bar = 'bar1'
var obj = { bar: 'bar2' }
foo() //'bar1'
foo.call(obj) //'bar2'

/** */

function foo() {
    console.log(this.bar)
}
var obj = { bar: 'bar' }
var obj2 = { bar: "bar2" }
var orig = foo
foo = function() {
    orig.call(obj)
}
foo() // 'bar'
foo.call(obj2) // 'bar'

/*** */

function bind(fn, o) {
    return function() {
        fn.call(o)
    }
}
function foo() {
    console.log(this.bar)
}
var obj = { bar: 'bar' }
var obj2 = { bar: 'bar2' }
foo = bind(foo,obj)
foo() //'bar'
foo.call(obj2) //'bar'


//1.'new' keyword
function foo() {
    this.baz = 'baz'
    console.log(this.bar + ' ' + baz)
}
var bar = 'bar'
var baz = new foo();

//what happens when new keyword is used?
//1 - a new object is created
//2 - that object is linked to other object
//3 - the new object will be referenced to this keyword
//4 - if there is no return, then it will return 'this'
